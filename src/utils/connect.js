import React, { Component, PropTypes } from 'react'
export function connect(options) {
  return (props) => <RebaseConnect {...options} {...props} />
}

class RebaseConnect extends Component {
  componentDidMount() {
    this.props.connections.forEach(this.connect.bind(this))
  }
  connect({ resource, isArray }) {
    this.context.base.syncState(resource, {
      context: this,
      state: resource,
      isArray
    })
  }

  render() {
    let { Component } = this.props

    return <Component {...this.state} />
  }
}

RebaseConnect.contextTypes = {
  base: PropTypes.any,
}