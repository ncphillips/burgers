import React, { Component, PropTypes } from 'react';
import {
  BrowserRouter as Router,
  Route,
} from 'react-router-dom'
import { BurgersPage } from '../pages/BurgersPage'
import { LoginPage } from '../pages/LoginPage'

import logo from './logo.svg';
import './App.css';

export class App extends Component {
  getChildContext() {
    return {
      base: this.props.base
    }
  }
  render() {
    return (
      <Router>
        <div>
          <Route exact path="/" component={BurgersPage} />
          <Route path="/login" component={LoginPage} />
        </div>
      </Router>
    );
  }
}

App.childContextTypes = {
  base: PropTypes.any,
}