import React, { Component } from 'react'
import './Burgers.css'

export class BurgersList extends Component {
  render() {
    return (
      <div className='BurgersList'>
        {
          this.props.burgers.map((burger, key) => <BurgerListing burger={burger} key={key} />)
        }

      </div>
    )
  }
}

class BurgerListing extends Component {
  render() {
    return (
      <div className="BurgerListing">{this.props.burger.name}</div>
    )
  }
}