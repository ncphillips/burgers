import React, { PropTypes, Component } from 'react'
import { BurgersList } from '../components/burgers'
import { connect } from '../utils/connect'

class BurgersPage extends Component {
  render() {
    return (
      <div>
        <h1>Burgers</h1>
        <BurgersList { ...this.props } />
      </div>
    )
  }
}

BurgersPage.defaultProps = {
  burgers: [],
}

let BurgersPageContainer = connect({
  Component: BurgersPage,
  connections: [
    { resource: 'burgers', asArray: true }
  ]
})

export { BurgersPageContainer as BurgersPage }

