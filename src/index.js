import React from 'react';
import ReactDOM from 'react-dom';
import { App }  from './App';
import './index.css';
import { base } from './config/firebase'

ReactDOM.render(
  <App base={base} />,
  document.getElementById('root')
);
